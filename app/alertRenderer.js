var alertRenderer = (function (renderer) {

    var alertsWrapper = null;
    var runingAlertCleaner = false;

    // show alertsWrapper, create alert and if necesery strart cleaning
    renderer.renderNewAlert = function (alert) {
        var alertsCount = alertsWrapper.children.length;
        var maxAlertsStored = 5;
        alertsWrapper.style.display = 'block';
        alertsWrapper.appendChild(this.createAlertItem(alert, (alertsCount)));
        if (alertsCount > (maxAlertsStored - 1)) {
            this.organizeAlerts();
        }
    };

    // init alertsWrapper
    renderer.initializeRoot = function () {
        if (!alertsWrapper) {
            alertsWrapper = document.getElementById('alertsWrapper');
        }
    }

    // async loop for cleaning alerts
    renderer.asyncLoop = function (o) {
        var loop = function () {
            if (!alertsWrapper.children.length) {
                o.callback(); return;
            }
            o.functionToLoop(loop);
        }
        loop();
    }

    // organize alerts by deleting last one and moving others down after cleaning done hide alertsWrapper
    renderer.organizeAlerts = function () {
        if (!runingAlertCleaner) {
            this.asyncLoop({
                functionToLoop: function (loop) {
                    runingAlertCleaner = true;
                    if (alertsWrapper.children[0]) {
                        setTimeout(function () {
                            var visibleAlerts = alertsWrapper.children;
                            var firstVisibleAlert = visibleAlerts[0];
                            firstVisibleAlert.style.opacity = '0';
                            setTimeout(function () {
                                for (i = 1; i < visibleAlerts.length; i++) {
                                    var actutalBottom = parseInt(visibleAlerts[i].style.bottom, 10);
                                    visibleAlerts[i].style.bottom = (actutalBottom - 50) + 'px';
                                }
                                if (typeof firstVisibleAlert.remove == 'function') {
                                    firstVisibleAlert.remove();
                                }
                                else {
                                    firstVisibleAlert.outerHTML = '';
                                }

                                loop();
                            }, 500);

                        }, 1000);
                    } else {
                        return false;
                    }

                },
                callback: function () {
                    alertsWrapper.style.display = 'none';
                    runingAlertCleaner = false;
                }
            });
        }
    }

    // create alert contant
    renderer.createAlertItem = function (alert, position) {

        var alertDiv = document.createElement("div");
        var innerTextBox = document.createElement("span");
        alertDiv.appendChild(innerTextBox);

        var distanceToMoveAlertBox = 50;
        innerTextBox.innerHTML = alert.description;
        alertDiv.className = 'alertBox';
        innerTextBox.className = 'textBox';
        alertDiv.style.bottom = position * distanceToMoveAlertBox + 'px';
        return alertDiv;
    }

    return renderer;

})(alertRenderer || {});