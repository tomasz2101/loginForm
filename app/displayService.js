var displayService = (function (display) {

    var html = null;
    var body = null;
    var centerWrapper = null;

    // necesary for mobile devices, situation when contant is bigger than mobile view
    display.initializeCentering = function () {
        var that = this;
        if (!html) {
            html = document.documentElement;
        }
        if (!body) {
            body = document.body;
        }
        if (!centerWrapper) {
            centerWrapper = document.getElementById('centerWrapper');
        }
        window.onresize = function (event) {
            that.changeBodyAndHtmlHeight();
        };
    }

    // change body and html height for mostly mobile devices
    display.changeBodyAndHtmlHeight = function () {
        var height = '100%';
        if (centerWrapper.offsetHeight > html.clientHeight) {
            height = 'auto';
        }
        html.style.height = height;
        body.style.height = height;
    }

    return display;

})(displayService || {});