var pageView = (function (view) {


    view.getAlerts = function () {
        return alertService.getAlerts();
    }

    view.addAlert = function (description) {
        alertService.addAlert(description);
    };

    view.keyEvent = function () {
        inputService.keyEvent();
    }

    view.submitValidation = function () {
        inputService.submitValidation();
    }

    view.initializeRoot = function () {
        alertService.initializeRoot();
    }

    view.initializeInputs = function () {
        inputService.initializeInputs();
    }

    view.initializePlaceholderStart = function () {
        inputService.initializePlaceholderStart();
    }

    view.initializeKeyEvents = function () {
        inputService.initializeKeyEvents();
    }

    view.initializeCentering = function () {
        displayService.initializeCentering();
        displayService.changeBodyAndHtmlHeight();
    }

    return view;

})(pageView || {});

document.addEventListener("DOMContentLoaded", function (event) {
    pageView.initializeCentering();
    pageView.initializeRoot();
    pageView.initializeInputs();
    pageView.initializeKeyEvents();
    pageView.initializePlaceholderStart();
    pageView.submitValidation();
    
});