var alertService = (function (service) {

    var alerts = [];

    // init alertsWrapper
    service.initializeRoot = function () {
        alertRenderer.initializeRoot();
    }

    // return all alerts
    service.getAlerts = function () {
        return alerts;
    }

    // return the newest alert
    service.getNewestAlert = function () {
        return alerts[alerts.length - 1]
    }

    // add new alert
    service.addAlert = function (description) {
        alerts.push(new Alert(description));
        alertRenderer.renderNewAlert(this.getNewestAlert());
    }
    return service;

})(alertService || {});