var inputService = (function (input) {

    var checkInputs = null;
    var submitButton = null;
    var successfulSubmit = true;

    // init inputs to validation
    input.initializeInputs = function () {
        if (!checkInputs) {
            checkInputs = document.getElementsByClassName('checkInput');
        } else {
            this.clearItems(checkInputs);
        }
        if (!submitButton) {
            submitButton = document.getElementById('submitButton');
        } else {
            this.clearItems(submitButton);
        }
    }

    // clear inputs itmes
    input.clearItems = function (element) {
        while (element.firstChild) {
            element.removeChild(element.firstChild);
        }
    }

    // init placholder if page has saved autofill inputs
    input.initializePlaceholderStart = function () {
        var that = this;
        setTimeout(function () {
            for (var i = 0; i < (checkInputs.length); i++) {
                that.initializePlaceholder(checkInputs[i]);
            }
        }, 100);
    }

    // init key events for input validation, enter is turned off
    input.initializeKeyEvents = function () {
        var that = this;
        for (var i = 0; i < (checkInputs.length); i++) {
            checkInputs[i].addEventListener('keyup', function (event) {
                that.initializePlaceholder(this);
                that.initializeOnlineCheckTyping(this);
            });
            checkInputs[i].addEventListener('keydown', function (event) {
                var key = event.which || event.keyCode;
                if (key === 13) {
                    event.preventDefault();
                    event.stopPropagation();
                }
            });
        }
    }

    // init online checling for exceeding char maximum input
    input.initializeOnlineCheckTyping = function (input) {
        if (input.classList.contains('checkTyping') && /^(.*?)(maxChar)\d/.test(input.className)) {
            var maxCharClass = input.className.match(/([^\?]*)\maxChar(\d*)/);
            var maxChar = maxCharClass[2];
            var margin = input.value.length - maxChar;
            var text = prepareErrorText(input, 'limit exceeded by ' + margin);
            checkLength(input, maxChar, text);
        }
    }

    // init placeholders 
    input.initializePlaceholder = function (input) {
        var holder = input.parentElement.querySelector('.holder');
        if (holder) {
            if (input.value.length > 0) {
                holder.style.display = 'none';
            } else {
                holder.style.display = 'block';
            }
        }
    }

    // validation on submiting, after successful validation store all inputs values into localStorage 
    input.submitValidation = function () {
        submitButton.addEventListener('click', function (event) {
            event.preventDefault();
            successfulSubmit = true;
            for (var i = 0; i < (checkInputs.length); i++) {
                var text = prepareErrorText(checkInputs[i], 'error');

                // checking not empty inputs
                if (checkInputs[i].classList.contains('notEmpty')) {
                    isEmpty(checkInputs[i], text);
                }

                // checking checkForNumbers
                if (checkInputs[i].classList.contains('noNumbers')) {
                    var type = 'noNumbers';
                    checkForNumbers(checkInputs[i], text, type);
                }

                // checking only numbers validation (vid number && tickets number)
                if (checkInputs[i].classList.contains('onlyNumbers')) {
                    var type = 'onlyNumbers'
                    checkForNumbers(checkInputs[i], text, type);
                }

                // checking max length
                if (/^(.*?)(maxChar)\d/.test(checkInputs[i].className)) {
                    var charLimit = checkInputs[i].className.match(/([^\?]*)\maxChar(\d*)/);
                    var num = charLimit[2];
                    checkLength(checkInputs[i], num, text);
                }

                // checking email validation
                if (checkInputs[i].classList.contains('emailValidation')) {
                    checkEmail(checkInputs[i], text);
                }

                // checking password validation
                if (checkInputs[i].classList.contains('passwordValidation')) {
                    checkPassword(checkInputs[i], text);
                }

                // checking max and min number
                if (/^(.*?)(Number)\d/.test(checkInputs[i].className)) {
                    var maxNumber = checkInputs[i].className.match(/([^\?]*)\maxNumber(\d*)/);
                    if (maxNumber) {
                        var num = maxNumber[2];
                        var type = 'max';
                        checkNumberLimit(checkInputs[i], num, type, text);
                    }

                    var minNumber = checkInputs[i].className.match(/([^\?]*)\minNumber(\d*)/);
                    if (minNumber) {
                        var num = minNumber[2];
                        var type = 'min';
                        checkNumberLimit(checkInputs[i], num, type, text);
                    }
                }
            }
            if (successfulSubmit) {
                inputService.addAlert('Wysłano');
                var saveData = {};
                for (var i = 0; i < (checkInputs.length); i++) {
                    if (checkInputs[i].type == 'radio') {
                        if (checkInputs[i].checked) {
                            saveData[checkInputs[i].getAttribute('name')] = checkInputs[i].value;
                        }
                    } else {
                        saveData[checkInputs[i].getAttribute('name')] = checkInputs[i].value;
                    }
                };

                // store all data as json object
                localStorage.setItem('submitForm', JSON.stringify(saveData));
            }
        });
    }

    // add alert with text error
    input.addAlert = function (errorText) {
        successfulSubmit = false;
        pageView.addAlert(errorText);
    }

    // checking number limit for input
    function checkNumberLimit(input, threshold, type, errorText) {
        if (input.value.length > 0) {
            if (type == 'min' && parseInt(input.value) < threshold) {
                inputService.addAlert(errorText);
            }
            if (type == 'max' && parseInt(input.value) > threshold) {
                inputService.addAlert(errorText);
            }
        }
    }

    // checking for numbers in input
    function checkForNumbers(input, errorText, type) {
        if (input.value.length > 0) {
            if (type == 'noNumbers' && checkReq(input.value, /\d+/)) {
                inputService.addAlert(errorText);
            }
            if (type == 'onlyNumbers' && !checkReq(input.value, /^[0-9]+$/)) {
                inputService.addAlert(errorText);
            }
        }
    }

    // checking password if contains at least 1 number 1 letter and 1 special sign, min 8 chars
    function checkPassword(input, errorText) {
        if (input.value.length > 0) {
            if (!checkReq(input.value, /^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[#?!@$%^&*-]).{8,}$/)) {
                inputService.addAlert(errorText);
            }
        }
    }

    // checking if email address is valid - > RFC 5322 Official Standard
    function checkEmail(input, errorText) {
        if (input.value.length > 0) {
            var rfc5322OfficialStandard = /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
            if (!checkReq(input.value, rfc5322OfficialStandard)) {
                inputService.addAlert(errorText);
            }
        }
    }

    function checkLength(input, threshold, errorText) {
        if (input.value.length > threshold) {
            inputService.addAlert(errorText);
            successfulSubmit = false;
        }
    }

    // preparing text aerror for each input
    function prepareErrorText(input, additionalText) {
        var holder = input.parentElement.querySelector('.holder');
        if (holder) {
            var errorText = holder.textContent;
            if (input.getAttribute('data-error')) {
                errorText = input.getAttribute('data-error');
            }
            errorText += ' ' + additionalText;
            return errorText;
        }
        return '';
    }

    function isEmpty(input, errorText) {
        if (!input.value.replace(/^\s+/g, '').length) {
            inputService.addAlert(errorText);
        }
    }

    function checkReq(str, reqex) {
        return reqex.test(str);
    }

    return input;

})(inputService || {});